# Adopting Menus and UIActions in your User Interface

Add menus to your user interface, with built-in button support and bar-button items, and create custom menu experiences.

## Overview

- Note: This sample code project is associated with WWDC20 session [10052: Build with iOS Pickers, Menus and Actions](https://developer.apple.com/wwdc20/10052/).


## iOS 앱 자동화 테스트 


샘플 앱 : https://developer.apple.com/documentation/uikit/menus_and_shortcuts/adopting_menus_and_uiactions_in_your_user_interface

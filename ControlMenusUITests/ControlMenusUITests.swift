//
//  ControlMenusUITests.swift
//  ControlMenusUITests
//
//  Created by 이령 on 2021/10/06.
//  Copyright © 2021 Apple. All rights reserved.
//

import XCTest

class ControlMenusUITests: XCTestCase {

    let app = XCUIApplication()
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
        
        // UI tests must launch the application that they test.
        app.launch()
        
        let mainButton = app.staticTexts["Demo Selector"]
        XCTAssertTrue(mainButton.waitForExistence(timeout: 5))
        mainButton.tap()
        
        UIView.setAnimationsEnabled(false)
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        
        app.buttons["Control Menus"].tap()
        XCTAssertTrue(app.staticTexts["Control Menus"].exists)
        XCTAssertTrue(app.staticTexts["Demo Selector"].exists)
        
        UIView.setAnimationsEnabled(true)
    }
    
    func testButtonDemo() throws {
        let buttonDemo = app.buttons["Button Demo"]
        XCTAssertTrue(buttonDemo.waitForExistence(timeout: 5))
        buttonDemo.tap()

        
        let menuButton = app.buttons["Menu Button"]
        menuButton.tap()
        
        
        
        let firstSwitch = app.switches.element(boundBy: 0)
        XCTAssertEqual(firstSwitch.value as! String, "0")
        firstSwitch.tap()
        XCTAssertEqual(firstSwitch.value as! String, "1")
        
        menuButton.tap()
        
//        XCTAssertTrue(app.staticTexts["Menu Triggered"].exists)
        let one = app.buttons["1"]
        XCTAssertTrue(one.waitForExistence(timeout: 5))
        one.tap()
        XCTAssertFalse(one.exists)
        
        menuButton.tap()
        let two = app.buttons["2"]
        XCTAssertTrue(two.waitForExistence(timeout: 5))
        two.tap()
        XCTAssertFalse(two.exists)
        
        
        menuButton.tap()
        let three = app.buttons["3"]
        XCTAssertTrue(three.waitForExistence(timeout: 5))
        three.tap()
        XCTAssertFalse(three.exists)
        
        
        menuButton.tap()
        let four = app.buttons["4"]
        XCTAssertTrue(four.waitForExistence(timeout: 5))
        four.tap()
        XCTAssertFalse(four.exists)
        
        
        menuButton.tap()
        let five = app.buttons["5"]
        XCTAssertTrue(five.waitForExistence(timeout: 5))
        five.tap()
        XCTAssertFalse(five.exists)
        
        
        firstSwitch.tap()
        XCTAssertEqual(firstSwitch.value as! String, "0")
    }
    
    func testButtonItemDemo() throws {
        let barButtonDemo = app.buttons["Bar Button Item Demo"]
        XCTAssertTrue(barButtonDemo.waitForExistence(timeout: 1))
        barButtonDemo.tap()
        XCTAssertTrue(app.staticTexts["UIBarButtonItem"].exists)
        
        let shareButton = app.buttons["Share"]
        XCTAssertTrue(shareButton.exists)
        shareButton.tap()
//        print(app.debugDescription)
//        XCTAssertTrue(app.staticTexts["Action Bar Button"].exists)
        
        
        let save = app.buttons["Save"]
        XCTAssertTrue(save.exists)
        save.tap()
//        XCTAssertTrue(app.staticTexts["Menu Action ''"].exists)
        
        
        let revert = app.buttons["Revert"]
        XCTAssertTrue(revert.exists)
        revert.tap()
//        XCTAssertTrue(app.staticTexts["Menu Action 'Revert'"].exists)
        
        
        let moreUp = app.buttons["more"].firstMatch
        moreUp.tap()
        let option1 = app.buttons["Option 1"]
        XCTAssertTrue(option1.waitForExistence(timeout: 5))
        option1.tap()
        XCTAssertFalse(option1.exists)
//        XCTAssertTrue(app.staticTexts["Menu Action 'Option 1'"].exists)
        
        
        moreUp.tap()
        let option2 = app.buttons["Option 2"]
        XCTAssertTrue(option2.waitForExistence(timeout: 5))
        option2.tap()
        XCTAssertFalse(option2.exists)
//        XCTAssertTrue(app.staticTexts["Menu Action 'Option 2'"].exists)
        
        
        moreUp.tap()
        let option3 = app.buttons["Option 3"]
        XCTAssertTrue(option3.waitForExistence(timeout: 5))
        option3.tap()
        XCTAssertFalse(option3.exists)
//        XCTAssertTrue(app.staticTexts["Menu Action 'Option 3'"].exists)
        
        
        moreUp.tap()
        let option4 = app.buttons["Option 4"]
        XCTAssertTrue(option4.waitForExistence(timeout: 5))
        option4.tap()
        XCTAssertFalse(option4.exists)
//        XCTAssertTrue(app.staticTexts["Menu Action 'Option 4'"].exists)
        
        
        moreUp.tap()
        let option5 = app.buttons["Option 5"]
        XCTAssertTrue(option5.waitForExistence(timeout: 5))
        option5.tap()
        XCTAssertFalse(option5.exists)
//        XCTAssertTrue(app.staticTexts["Menu Action 'Option 5'"].exists)
        
        
        let app = XCUIApplication()
        let moreButton = app.toolbars["Toolbar"].buttons["more"]
        moreButton.tap()
        
        let collectionViewsQuery = app.collectionViews
        let Collaborate = collectionViewsQuery.cells.buttons["Collaborate"]
        XCTAssertTrue(Collaborate.waitForExistence(timeout: 5))
        Collaborate.tap()
//        XCTAssertTrue(app.staticTexts["Menu Action 'Collaborate'"].exists)
        
        
        moreButton.tap()
        let cellShare = collectionViewsQuery.cells.buttons["Share"]
        XCTAssertTrue(cellShare.waitForExistence(timeout: 5))
        cellShare.tap()
//        XCTAssertTrue(app.staticTexts["Menu Action 'Share'"].exists)'
        
        
        moreButton.tap()
        let Info = collectionViewsQuery.cells.buttons["Info"]
        XCTAssertTrue(Info.waitForExistence(timeout: 5))
        Info.tap()
//        XCTAssertTrue(app.staticTexts["Menu Action 'Info'"].exists)
    }
    
    func testBackButtonDemo() throws {
        let backButtonDemo = app.buttons["Back Button Demo"]
        XCTAssertTrue(backButtonDemo.waitForExistence(timeout: 5))
        backButtonDemo.tap()

        XCTAssertTrue(app.staticTexts["Level 10"].exists)
        XCTAssertTrue(app.buttons["9"].exists)
        app.buttons["9"].tap()
        XCTAssertTrue(app.staticTexts["Level 9"].exists)
        
        app.buttons["8"].tap()
        XCTAssertTrue(app.staticTexts["Level 8"].exists)
        
        app.buttons["7"].tap()
        XCTAssertTrue(app.staticTexts["Level 7"].exists)
        
        app.buttons["6"].tap()
        XCTAssertTrue(app.staticTexts["Level 6"].exists)
        
        app.buttons["5"].tap()
        XCTAssertTrue(app.staticTexts["Level 5"].exists)
        
        app.buttons["4"].tap()
        XCTAssertTrue(app.staticTexts["Level 4"].exists)
        
        app.buttons["3"].tap()
        XCTAssertTrue(app.staticTexts["Level 3"].exists)
        
        app.buttons["2"].tap()
        XCTAssertTrue(app.staticTexts["Level 2"].exists)
        
        app.buttons["1"].tap()
        XCTAssertTrue(app.staticTexts["Level 1"].exists)
    }
    
    func testSegmentedControlDemo() throws {
        let segmentedButtonDemo = app.buttons["Segmented Control Demo"]
        XCTAssertTrue(segmentedButtonDemo.waitForExistence(timeout: 5))
        segmentedButtonDemo.tap()
        
        
        app.segmentedControls.buttons["Blue"].tap()
        app.segmentedControls.buttons["Green"].tap()
        app.segmentedControls.buttons["Red"].tap()
        app.segmentedControls.buttons["Shield"].tap()
        app.segmentedControls.buttons["Capsule"].tap()
        app.segmentedControls.buttons["Seal"].tap()
                
                
    }
    
    func testDeferredElementDemo() throws {
        let deferredEleDemo = app.buttons["Deferred Element Demo"]
        XCTAssertTrue(deferredEleDemo.waitForExistence(timeout: 5))
        deferredEleDemo.tap()
        
        
        let menuButton = app.buttons["Menu Button"]
        XCTAssertTrue(menuButton.waitForExistence(timeout: 5))
        menuButton.tap()
        let staticItem1 = app.buttons["Static Item 1"]
        XCTAssertTrue(staticItem1.waitForExistence(timeout: 5))
        staticItem1.tap()
        XCTAssertFalse(staticItem1.exists)
        
        
        XCTAssertTrue(menuButton.waitForExistence(timeout: 5))
        menuButton.tap()
        let staticItem2 = app.buttons["Static Item 2"]
        XCTAssertTrue(staticItem2.waitForExistence(timeout: 5))
        staticItem2.tap()
        XCTAssertFalse(staticItem2.exists)
        

        XCTAssertTrue(menuButton.waitForExistence(timeout: 5))
        menuButton.tap()
        let dynamicItem1 = app.buttons["Dynamic Item 1"]
        XCTAssertTrue(dynamicItem1.waitForExistence(timeout: 5))
        dynamicItem1.tap()
        XCTAssertFalse(dynamicItem1.exists)
        
        
        XCTAssertTrue(menuButton.waitForExistence(timeout: 5))
        menuButton.tap()
        let dynamicItem2 = app.buttons["Dynamic Item 2"]
        XCTAssertTrue(dynamicItem2.waitForExistence(timeout: 5))
        dynamicItem2.tap()
        XCTAssertFalse(dynamicItem2.exists)
    }
    
    func testUpdateMenuDemo() throws {
        let updateMenuDemo = app.buttons["Update Menu Demo"]
        XCTAssertTrue(updateMenuDemo.waitForExistence(timeout: 5))
        updateMenuDemo.tap()
        
        XCTAssertTrue(app.staticTexts["Updating Menu"].exists)
    }

//    func testLaunchPerformance() throws {
//        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, watchOS 7.0, *) {
//            // This measures how long it takes to launch your application.
//            measure(metrics: [XCTApplicationLaunchMetric()]) {
//                XCUIApplication().launch()
//            }
//        }
//    }
}
